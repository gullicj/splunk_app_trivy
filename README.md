# splunk_app_trivy

## indexes
trivy
trivy_dbupdates

## apps
trivy (lives on deployment server)
trivy_collect (deployment-app)

## about offline db's / airgapped environments
https://aquasecurity.github.io/trivy/v0.17.2/air-gap/