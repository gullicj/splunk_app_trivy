#!/bin/bash

# documented way to update db, but "latest" path on github is inaccurate for this project right now (returns end of life v1 release)
# wget https://github.com/aquasecurity/trivy-db/releases/latest/download/trivy-offline.db.tgz

# workaround
# update cached db on this host
/export/splunk/etc/deployment-apps/trivy_collect/bin/trivy image --download-db-only

# create tgz of db in deployment app from cached db on this host
tar -czf /export/splunk/etc/deployment-apps/trivy_collect/db/offlinedb.tgz $HOME/.cache/trivy/db