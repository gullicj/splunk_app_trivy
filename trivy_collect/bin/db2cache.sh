#!/bin/bash

# define and, if needed, create cache directory for trivy in user home
TRIVY_CACHE="$HOME/.cache/trivy"
[ ! -d "$HOME/.cache/trivy" ] && mkdir -p "$TRIVY_CACHE"

# copy and extract offline trivy db to trivy cache
cp $SPLUNK_HOME/etc/apps/trivy_collect/db/offlinedb.tgz $TRIVY_CACHE
cd $TRIVY_CACHE
tar xzf $TRIVY_CACHE/offlinedb.tgz